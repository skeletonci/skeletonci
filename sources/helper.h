#pragma once

#include <string>

class MessageHelper {
public:
    static std::string buildMessage();
};
